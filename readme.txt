dbug
-----------------
This module makes the dBug tool available to the Drupal site. This allows developers to call the function from other modules.

NOTE: The function implemented is named dbug() not dBug(). This was done simply to make it easier to type.

dBug.php source code provided from http://dbug.ospinto.com/